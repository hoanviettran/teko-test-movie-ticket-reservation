# Backend module for movie tickets reservation

## DEFAULT
==== Test Room: RoomId:300000, ColumnsCount:10, RowsCount:10 ===

++++ Test Screening: ScreeningId: 500000, Min distance: 3 ++++

## RUN
```
go run main.go
```

- Create a screening
```
curl --location --request POST 'localhost:9025/api/v1.0/screening' \
--header 'Content-Type: application/json' \
--data-raw '{   
    "RoomId": 351817,
    "MinDistance": 3
}'
```

- Get list seats of the screening
```
curl --location --request GET 'localhost:9025/api/v1.0/screening/532408/reservation'
```
- Reserve a list of seats
``` 
curl --location --request POST 'localhost:9025/api/v1.0/reservation' \
--header 'Content-Type: application/json' \
--data-raw '{
    "ScreeningId": 500000,
    "ListSeatIds": [152275, 194802]
}'
```
- Return a reservation
``` 
curl --location --request DELETE 'localhost:9025/api/v1.0/reservation/:reservationId'
```

