package constant

import "time"

// constant for expire time
const (
	CacheExpiresInForever        = 0
	CacheExpiresInOneMinute      = time.Minute
	CacheExpiresInTenMinutes     = time.Minute * 10
	CacheExpiresInFifteenMinutes = time.Minute * 15
	CacheExpiresInOneHour        = time.Hour
	CacheExpiresInTwoHour        = CacheExpiresInOneHour * 2
	CacheExpiresInOneDay         = CacheExpiresInOneHour * 24
	CacheExpiresInThreeDay       = CacheExpiresInOneDay * 3
	CacheExpiresInOneMonth       = CacheExpiresInOneDay * 30
	CacheExpiresInThreeMonths    = CacheExpiresInOneMonth * 3
	CacheExpiresInSixMonths      = CacheExpiresInOneMonth * 6
	CacheExpiresInOneYear        = CacheExpiresInOneMonth * 12
)

// constant for aoe
const (
	CacheAoeRoomInfoPrefix         = "backend:game-offline:aoe:room-info:"
	CacheAoeRoomBlockingFlagPrefix = "backend:game-offline:aoe:room-blocking:"

	CacheAoeListAllRooms             = "backend:game-offline:aoe:rooms:all"
	CacheAoeListRoomsPrefix          = "backend:game-offline:aoe:rooms"
	CacheAoeListAvailableRoomsPrefix = "backend:game-offline:aoe:available-rooms:"
	CacheAoeListRoomsOfModePrefix    = "backend:game-offline:aoe:available-rooms:mode:"
	CacheAoeListRoomsOfTypePrefix    = "backend:game-offline:aoe:available-rooms:type:"

	CacheAoeZonesInfoPrefix  = "backend:game-offline:aoe:zone:"
	CacheAoeGroupInfoPrefix  = "backend:game-offline:aoe:group:"
	CacheAoeListZones        = "backend:game-offline:aoe:zones"
	CacheAoeListGroupsPrefix = "backend:game-offline:aoe:groups:"

	CacheAoeListWaitingRooms            = "backend:game-offline:aoe:rooms:waiting"         // lobby join room
	CacheAoeListStartingRooms           = "backend:game-offline:aoe:rooms:starting"        // member start game
	CacheAoeRoomListStartSuccessPlayers = "backend:game-offline:aoe:success-players:room:" // member start game

	CacheListFindingLobbiesPrefix = "backend:game-offline:aoe:finding-lobbies:"
	CacheLobbyDataPrefix          = "backend:game-offline:aoe:lobby:"
	MatchingCommandQueue          = "backend:game-offline:aoe:finding-match-commands"
)

// constant for server list
const (
	CacheServerInfoPrefix  = "backend:game-offline:server"
	CacheListServersPrefix = "backend:game-offline:servers"
)

// user
const (
	CacheAoeOnlineMembers       = "backend:game-offline:aoe:online-members"
	CacheCSOnlineMembers        = "backend:game-offline:cs:online-members"
	CacheWarscraftOnlineMembers = "backend:game-offline:warcraft:online-members"

	CacheAoeListUsersGroupPrefix = "backend:game-offline:aoe:users:group:"
	CacheAoeUserCodeServerPrefix = "backend:game-offline:aoe:users:server:"
	// openid
	CacheListOnlineUsers string = "backend-online-users"
)

// constant for max person in one server
const (
	MaxPersonInOneServer = 10000
)

// constant for cs member
const (
	CacheAoeUserCurrentRoomPrefix  = "backend:game-offline:aoe:user-current-room:"
	CacheAoeUserCurrentLobbyPrefix = "backend:game-offline:aoe:user-current-lobby:"

	CacheUserGameStatusPrefix = "backend:game-offline:user:"
	CacheCurrentGameField     = "Game"
	CacheCurrentRoomField     = "RoomId"
	CacheCurrentGroupField    = "GroupId"
	CacheCurrentLobbyField    = "LobbyId"
	CacheCurrentServerField   = "ServerIP"
)

// constant for open id
const (
	CacheUserSessionPrefix           string = "backend-user-session:"
	CacheUserSessionTokenField       string = "token"
	CacheUserSessionMobileTokenField string = "mobile-token"

	CacheUserGamificationProfileData string = "gtv:bk:user-profile:"
	CacheAccountPackageTypeField     string = "AccountType"
	CacheExpMoneyField               string = "exp-money"
)

// constant for report
const (
	MaxReportPerDay      int    = 3
	CacheReportUsedToday string = "backend-game-report"
)
