package constant

const (
	ReservationStatusCreated int = 1
	ReservationStatusCompleted int = 5
	ReservationStatusReturned int = 7
)

const (
	SeatReservationStatusAvailable int = 0
	SeatReservationStatusUnAvailable int = 1
	SeatReservationStatusProcessing int = 3
	SeatReservationStatusOwned int = 5
)

const (
	TaskActionUpdateSeats   int = 1
	TaskActionReserveSeat 	int = 5
	TaskActionReturnSeat 	int = 7
)

const (
	ObjectKindScreening string = "screening"
	ObjectKindReservation string = "reservation"
	ObjectKindRoom string = "room"
	ObjectKindSeat string = "seat"
)