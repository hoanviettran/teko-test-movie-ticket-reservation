package constant

const (
	TableNameServer string = "game_offline_server"

	TableNameStatisticLogUserEvent string = "statistic_log_user_event"
	TableNameStatisticUserEvent string = "statistic_user_event"
)

const (
	TableNameGroup string = "game_offline_group"
	TableNameZone string = "game_offline_zone"
)