package dto

type MovieScreening struct {
	ScreeningId int `json:"ScreeningId"`
	MovieId     int `json:"MovieId"`
	StartAt     int `json:"StartAt"`
	EndAt       int `json:"EndAt"`
	// room
	RoomId int `json:"RoomId"`
	// seat
	MinDistance      int            `json:"MinDistance"`
	AvailableSeats   map[string]int `json:"-"`
	UnAvailableSeats map[string]int `json:"-"`

	CreatedAt int64 `json:"CreatedAt"`
	UpdatedAt int64 `json:"UpdatedAt"`
}

type Room struct {
	RoomId       int `json:"RoomId"`
	RowsCount    int `json:"RowsCount"`
	ColumnsCount int `json:"ColumnsCount"`

	CreatedAt int64 `json:"CreatedAt"`
	UpdatedAt int64 `json:"UpdatedAt"`
}

type Seat struct {
	SeatId   int    `json:"SeatId"`
	SeatName string `json:"SeatName"`
	RoomId   int    `json:"RoomId"`
	RowId    int    `json:"RowId"`
	ColumnId int    `json:"ColumnId"`
}

type Reservation struct {
	ReservationId          int      `json:"ReservationId"`
	ScreeningId            int      `json:"ScreeningId"`
	CreditAmount           int      `json:"CreditAmount"`
	ListSeatIds            []int    `json:"ListSeatIds"`
	ListSeatReservationIds []string `json:"-"`
	Status                 int      `json:"Status"`
}

type SeatReservation struct {
	ScreeningId       int    `json:"ScreeningId"`
	ReservationId     int    `json:"ReservationId"`
	SeatReservationId string `json:"SeatReservationId"`
	SeatId            int    `json:"SeatId"`
	ColumnId          int    `json:"ColumnId"`
	RowId             int    `json:"RowId"`
	Status            int    `json:"SeatStatus"`
	IsBlockedBy       map[string]int
}
