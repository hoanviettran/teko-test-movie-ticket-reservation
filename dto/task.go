package dto

type Task struct {
	//ScreeningId       int    `json:"ScreeningId"`
	SeatReservationId string `json:"SeatReservationId"`
	IsBlockedBy       string `json:"IsBlockedBy"`
	Action 			  int 		`json:"Action"`
}
