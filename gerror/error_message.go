package gerror

func T(errorCode uint32) string {
	switch errorCode {
	//////////////////////////
	// Client-side
	//////////////////////////
	case ErrorBindData:
		return "Failed to bind data"
	case ErrorValidData:
		return "Failed to valid data"

	//////////////////////////
	// Server-side
	//////////////////////////
	case ErrorConnect:
		return "Failed to connect database"
	case ErrorProcessing:
		return "Failed to process request"
	case ErrorRetrieveData:
		return "Failed to retrieve data"
	case ErrorLogin:
		return "Failed to login. Please try again!"
	}

	return "Unknown error"
}
