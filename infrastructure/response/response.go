package response

import (
	"custom-lib/gerror"
)

/**
 * Defines a response object
 */
type Response struct {
	Message string      `json:"Message"`
	Data    interface{} `json:"Data"`
}

/**
 * Defines an error response object
 */
type ErrorResponse struct {
	ErrorCode uint32 `json:"ErrorCode"`
	Message   string `json:"Message"`
	Exception string `json:"Exception"`
}

/**
 * Returns a new error response object
 */
func NewErrorResponse(errorCode uint32, message string, exception string) (string, ErrorResponse) {
	msg := gerror.T(errorCode)
	// status := errorCode
	err := ErrorResponse{
		ErrorCode: errorCode,
		Message:   message,
	}

	return msg, err
}
