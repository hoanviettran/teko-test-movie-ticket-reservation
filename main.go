package main

import (
	"fmt"
	"custom-lib/module/cinema"
	"custom-lib/module/healthcheck"
	"time"
	_ "github.com/lib/pq"
	"custom-lib/infrastructure/logger"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile(`config.json`)

	err := viper.ReadInConfig()

	if err != nil {
		fmt.Println("Read config error: ", err)
		panic(err)
	}

	if viper.GetBool(`Debug`) {
		fmt.Println("Service RUN on DEBUG mode")
	} else {
		fmt.Println("Service RUN on PRODUCTION mode")
	}
}

func main() {
	/********************************************************************/
	/* CONFIGURE LOG                          */
	/********************************************************************/
	logPath := viper.GetString("Log.Path")
	logPrefix := viper.GetString("Log.Prefix")
	logger.NewLogger(logPath, logPrefix)

	/********************************************************************/
	/* CONFIGURE ECHO													*/
	/********************************************************************/
	timeout := time.Duration(viper.GetInt("Context.Timeout")) * time.Second

	e := echo.New()

	// Set timeout and disable keep alive
	e.Server.SetKeepAlivesEnabled(false)
	e.Server.ReadTimeout = timeout
	e.Server.WriteTimeout = timeout

	e.Use(middleware.CORS())

	healthcheck.Initialize(e, timeout)
	cinema.Initialize(e, timeout)

	// Start application
	e.Start(viper.GetString("Server.Address"))
}
