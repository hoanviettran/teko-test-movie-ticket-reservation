package controller

import (
	"context"
	"custom-lib/dto"
	"custom-lib/gerror"
	"custom-lib/infrastructure/controller"
	"custom-lib/infrastructure/response"
	"custom-lib/infrastructure/util"
	"custom-lib/module/cinema/service"
	"github.com/labstack/echo"
)

type SeatController struct {
	controller.BaseController
	SeatService service.ISeatService
}

func (controller *SeatController) CreateScreeningCtl(c echo.Context) error {
	var screening dto.MovieScreening
	err := c.Bind(&screening)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorBindData, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	screening, err = controller.SeatService.CreateNewMovieScreening(screening)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorProcessing, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	// 3. return
	return controller.WriteSuccess(c, screening)
}


func (controller *SeatController) GetListScreenings(c echo.Context) error {

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	// 2. retrieve data
	listScreenings, err := controller.SeatService.GetListScreenings()
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorProcessing, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	// 3. return
	return controller.WriteSuccess(c, listScreenings)
}

func (controller *SeatController) GetListSeatsOfScreenings(c echo.Context) error {

	screeningId := util.ParseInt(c.Param("screeningId"))

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	// 2. retrieve data
	listSeats, err := controller.SeatService.GetListSeatsOfScreening(screeningId, -1)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorProcessing, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	// 3. return
	return controller.WriteSuccess(c, listSeats)
}


func (controller *SeatController) ReserveSeats(c echo.Context) error {
	var reservation dto.Reservation
	err := c.Bind(&reservation)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorBindData, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	// 2. retrieve data
	err = controller.SeatService.CreateNewReservation(&reservation)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorProcessing, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	// 3. return
	return controller.WriteSuccess(c, reservation)
}


func (controller *SeatController) ReturnReservation(c echo.Context) error {
	reservationId := util.ParseInt(c.Param("reservationId"))

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	// 2. retrieve data
	err := controller.SeatService.ReturnReservation(reservationId)
	if err != nil {
		message, errRes := response.NewErrorResponse(gerror.ErrorBindData, err.Error(), "")
		return controller.WriteBadRequest(c, message, errRes)
	}

	// 3. return
	return controller.WriteSuccessEmptyContent(c)
}

