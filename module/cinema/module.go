package cinema

import (
	"custom-lib/module/cinema/controller"
	"custom-lib/module/cinema/service"
	"time"
	"github.com/labstack/echo"

)

var mSeatController *controller.SeatController

/**
 * Initializes module
 */
func Initialize(e *echo.Echo, timeout time.Duration, ) {

	mSeatService := service.NewSeatService(timeout)
	mSeatController = &controller.SeatController{
		SeatService:mSeatService,
	}
	go func() {
		mSeatService.Consumer()
	}()

	// New router
	InitRoute(e)
}

func InitRoute(e *echo.Echo) {

	e.POST("/api/v1.0/screening", mSeatController.CreateScreeningCtl)

	e.GET("/api/v1.0/screening", mSeatController.GetListScreenings)
	e.GET("/api/v1.0/screening/:screeningId/reservation", mSeatController.GetListSeatsOfScreenings)

	e.POST("/api/v1.0/reservation", mSeatController.ReserveSeats)
	e.DELETE("/api/v1.0/reservation/:reservationId", mSeatController.ReturnReservation)
}

