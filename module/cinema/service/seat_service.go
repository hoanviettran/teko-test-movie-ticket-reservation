package service

import (
	"custom-lib/constant"
	"custom-lib/dto"
	"custom-lib/infrastructure/logger"
	"fmt"
	"github.com/fatih/structs"
	"math"
	"math/rand"
	"sort"
	"time"
)

var mapSeats = make(map[int]dto.Seat)
var mapRooms = make(map[int]dto.Room)
var mapSeatsOfRoom = make(map[int]map[int]dto.Seat)
var mapScreenings = make(map[int]dto.MovieScreening)
var mapReservation = make(map[int]dto.Reservation)

var mapSeatReservations = make(map[string]dto.SeatReservation)

var taskChannel = make(chan dto.Task)
var mapBase = make(map[string]int)

type ISeatService interface {
	GetListScreenings() ([]dto.MovieScreening, error)
	CreateNewMovieScreening(screening dto.MovieScreening) (dto.MovieScreening, error)

	CreateNewRoom(room dto.Room) (dto.Room, error)

	GetListSeatsOfScreening(screeningId int, numSeats int) ([]dto.SeatReservation, error)

	CreateNewReservation(reservation *dto.Reservation) error
	ReturnReservation(reservationId int) error

	Consumer()
}

type SeatService struct {
	Timeout time.Duration
}

// Return a new cache service
func NewSeatService(timeout time.Duration) ISeatService {
	groupService := SeatService{
		Timeout: timeout,
	}

	mapBase[constant.ObjectKindReservation] = 9
	mapBase[constant.ObjectKindScreening] = 5
	mapBase[constant.ObjectKindRoom] = 3
	mapBase[constant.ObjectKindSeat] = 1
	// use for test // Init seats, rooms
	testRoom, _ := groupService.CreateNewRoom(dto.Room{
		RoomId: 300000,
		RowsCount:    10,
		ColumnsCount: 10,
	})
	fmt.Println(" ==== Test Room ", structs.Map(testRoom))
	screening, _ := groupService.CreateNewMovieScreening(dto.MovieScreening{
		ScreeningId: 500000,
		RoomId:      testRoom.RoomId,
		MinDistance: 3,
	})
	fmt.Printf(" ++++++++ Test Screening: %v, Min distance: %v ++++ ", screening.ScreeningId, screening.MinDistance)

	return &groupService
}

func (service *SeatService) GetListScreenings() ([]dto.MovieScreening, error) {
	var listScreenings []dto.MovieScreening
	for _, screening := range mapScreenings {
		listScreenings = append(listScreenings, screening)
	}
	return listScreenings, nil
}

func (service *SeatService) CreateNewMovieScreening(screening dto.MovieScreening) (dto.MovieScreening, error) {
	screening.CreatedAt = time.Now().Unix()
	screening.UpdatedAt = screening.CreatedAt
	if screening.ScreeningId == 0 {
		screening.ScreeningId = service.GenId("screening")
	}

	room := mapRooms[screening.RoomId]

	screening.UnAvailableSeats = make(map[string]int)
	screening.AvailableSeats = make(map[string]int)

	for _, seat := range mapSeatsOfRoom[room.RoomId] {
		seatReservationId := service.GenSeatReservationId(screening.ScreeningId, seat.ColumnId, seat.RowId)
		screening.AvailableSeats[seatReservationId] = 1
		mapSeatReservations[seatReservationId] = dto.SeatReservation{
			SeatReservationId: seatReservationId,
			SeatId:            seat.SeatId,
			ColumnId:          seat.ColumnId,
			RowId:             seat.RowId,
			ScreeningId:       screening.ScreeningId,
			Status:            constant.SeatReservationStatusAvailable,
			IsBlockedBy:       make(map[string]int),
		}
	}

	mapScreenings[screening.ScreeningId] = screening

	return screening, nil
}

func (service *SeatService) GenId(objectKind string) int {
	rand.Seed(time.Now().UnixNano())
	var objectId int
	for {
		objectId = rand.Intn(1e5) + mapBase[objectKind]*1e5
		switch objectKind {
		case constant.ObjectKindReservation:
			if _, ok := mapReservation[objectId]; !ok {
				return objectId
			}
		case constant.ObjectKindScreening:
			if _, ok := mapScreenings[objectId]; !ok {
				return objectId
			}
		case constant.ObjectKindRoom:
			if _, ok := mapRooms[objectId]; !ok {
				return objectId
			}
		case constant.ObjectKindSeat:
			if _, ok := mapSeats[objectId]; !ok {
				return objectId
			}
		}
	}
}

func (service *SeatService) GenSeatReservationId(screeningId int, columnId int, rowId int) string {
	return fmt.Sprintf("%v:%v-%v", screeningId, columnId, rowId)
}

func (service *SeatService) CreateNewRoom(room dto.Room) (dto.Room, error) {
	room.CreatedAt = time.Now().Unix()
	room.UpdatedAt = room.CreatedAt
	if room.RoomId == 0 {
		room.RoomId = service.GenId("room")
	}

	mapRooms[room.RoomId] = room
	mapSeatsOfRoom[room.RoomId] = make(map[int]dto.Seat)

	for x := 0; x < room.ColumnsCount; x++ {
		for y := 0; y < room.RowsCount; y++ {
			seat := dto.Seat{
				SeatId:   service.GenId(constant.ObjectKindSeat),
				RoomId:   room.RoomId,
				RowId:    y,
				ColumnId: x,
			}
			mapSeats[seat.SeatId] = seat
			mapSeatsOfRoom[room.RoomId][seat.SeatId] = seat
		}
	}

	return room, nil
}

func (service *SeatService) CreateNewReservation(reservation *dto.Reservation) error {
	var err error
	// Check list seats still available
	for _, seatId := range reservation.ListSeatIds {
		seat := mapSeats[seatId]
		userSeatReservationId := service.GenSeatReservationId(reservation.ScreeningId, seat.ColumnId, seat.RowId)
		reservation.ListSeatReservationIds = append(reservation.ListSeatReservationIds, userSeatReservationId)
		// check seat
		if _, ok := mapScreenings[reservation.ScreeningId].AvailableSeats[userSeatReservationId]; !ok {
			err = fmt.Errorf("[Seat] - reserve an unavailable seat, screen: %v, col: %v, row: %v",
				reservation.ScreeningId, seat.ColumnId, seat.RowId)
			break
		}
	}
	reservation.ReservationId = service.GenId(constant.ObjectKindReservation)
	reservation.Status = constant.ReservationStatusCompleted
	if err == nil {
		for _, seatId := range reservation.ListSeatIds {
			service.ReserveSeat(reservation.ReservationId, reservation.ScreeningId, seatId, true)
		}
		mapReservation[reservation.ReservationId] = *reservation
	}

	return err
}

func (service *SeatService) ReserveSeat(reservationId int, screeningId int, seatId int, force bool) error {

	seat := mapSeats[seatId]
	columnId := seat.ColumnId
	rowId := seat.RowId
	logger.Info("[Reservation] - reserve a seat, reservationId: %v, screeningId: %v, seat: %v, force: %v",
		reservationId, screeningId, seatId, force)
	userSeatReservationId := service.GenSeatReservationId(screeningId, columnId, rowId)

	if !force {
		// check seat
		if _, ok := mapScreenings[screeningId].AvailableSeats[userSeatReservationId]; !ok {
			logger.Error("[Seat] - reserve an unavailable seat, screen: %v, col: %v, row: %v", screeningId, columnId, rowId)
			return nil
		}
	}

	delete(mapScreenings[screeningId].AvailableSeats, userSeatReservationId)
	taskChannel <- dto.Task{
		SeatReservationId: userSeatReservationId,
		Action:            constant.TaskActionReserveSeat,
	}

	effectSeats := service.FindEffectSeats(screeningId, columnId, rowId)
	for _, seatReservationId := range effectSeats {
		// mark this seat is blocked by
		mapSeatReservations[seatReservationId].IsBlockedBy[userSeatReservationId] = 1

		delete(mapScreenings[screeningId].AvailableSeats, seatReservationId)
		mapScreenings[screeningId].UnAvailableSeats[seatReservationId] += 1
		taskChannel <- dto.Task{
			SeatReservationId: seatReservationId,
			Action:            constant.TaskActionUpdateSeats,
			IsBlockedBy:       userSeatReservationId,
		}
	}
	return nil
}

func (service *SeatService) FindEffectSeats(screeningId int, columnId int, rowId int) []string {
	screening := mapScreenings[screeningId]
	room := mapRooms[screening.RoomId]
	effectSeats := []string{}
	minX := columnId - screening.MinDistance
	maxX := columnId + screening.MinDistance
	for x := 0; x < room.ColumnsCount; x++ {
		if x < minX || x > maxX {
			continue
		}
		delta := int(math.Min(float64(x-minX), float64(maxX-x)))
		for y := 0; y < room.RowsCount; y++ {
			if y < rowId-delta || y > rowId+delta {
				continue
			}
			if x != columnId || y != rowId {
				effectSeats = append(effectSeats, service.GenSeatReservationId(screeningId, x, y))
			}
		}
	}
	return effectSeats
}

func (service *SeatService) ReturnReservation(reservationId int) error {
	var err error
	reservation := mapReservation[reservationId]
	logger.Info("[Reservation] - Return a reservation: %v", reservation)
	if reservation.Status != constant.ReservationStatusCompleted {
		return nil
	}
	for _, seatReservationId := range reservation.ListSeatReservationIds {
		service.ReturnSeat(mapSeatReservations[seatReservationId])
	}
	reservation.Status = constant.ReservationStatusReturned
	mapReservation[reservationId] = reservation
	return err
}

func (service *SeatService) ReturnSeat(returnSeatReservation dto.SeatReservation) error {

	logger.Info("[Seat] - Return seat: %v", returnSeatReservation)
	returnSeatReservationId := returnSeatReservation.SeatReservationId
	returnSeatReservation = mapSeatReservations[returnSeatReservationId]
	if returnSeatReservation.Status < constant.SeatReservationStatusOwned {
		// not reserved
		logger.Error("[Seat] - Return an invalid seat: %v", returnSeatReservation)
		//return nil
	}

	screeningId := returnSeatReservation.ScreeningId
	seat := mapSeats[returnSeatReservation.SeatId]

	delete(mapScreenings[screeningId].UnAvailableSeats, returnSeatReservationId)
	mapScreenings[screeningId].AvailableSeats[returnSeatReservationId] = 1

	effectSeats := service.FindEffectSeats(screeningId, seat.RowId, seat.ColumnId)
	for _, seatReservationId := range effectSeats {
		taskChannel <- dto.Task{
			SeatReservationId: seatReservationId,
			Action:            constant.TaskActionReturnSeat,
			IsBlockedBy:       returnSeatReservationId,
		}
	}
	return nil
}

func (service *SeatService) GetListSeatsOfScreening(screeningId int, numSeats int) ([]dto.SeatReservation, error) {
	listSeats := []dto.SeatReservation{}
	screening := mapScreenings[screeningId]

	for seatId, _ := range mapSeatsOfRoom[screening.RoomId] {
		seat := mapSeats[seatId]
		seatReservation := mapSeatReservations[service.GenSeatReservationId(screeningId, seat.ColumnId, seat.RowId)]
		seatReservation.SeatId = seatId
		listSeats = append(listSeats, seatReservation)
	}

	sort.Slice(listSeats, func(i, j int) bool {
		if listSeats[i].RowId == listSeats[j].RowId {
			return listSeats[i].ColumnId < listSeats[j].ColumnId
		} else {
			return listSeats[i].RowId < listSeats[j].RowId
		}
	})

	return listSeats, nil
}

func (service *SeatService) Consumer() {
	for {
		c := <-taskChannel
		service.HandleTask(&c)
	}
}

func (service *SeatService) HandleTask(task *dto.Task) {
	logger.Info("Handle task: %v", task)
	if task.Action == constant.TaskActionUpdateSeats {
		// mark this seat now is unavailable
		service.UpdateSeatReservationStatus(task.SeatReservationId, constant.SeatReservationStatusUnAvailable)
	} else if task.Action == constant.TaskActionReturnSeat {
		delete(mapSeatReservations[task.SeatReservationId].IsBlockedBy, task.IsBlockedBy)
		if len(mapSeatReservations[task.SeatReservationId].IsBlockedBy) == 0 {
			service.UpdateSeatReservationStatus(task.SeatReservationId, constant.SeatReservationStatusAvailable)
		}
	} else if task.Action == constant.TaskActionReserveSeat {
		service.UpdateSeatReservationStatus(task.SeatReservationId, constant.SeatReservationStatusOwned)
	}
}

func (service *SeatService) UpdateSeatReservationStatus(seatReservationId string, status int) {
	seatTmp := mapSeatReservations[seatReservationId]
	seatTmp.Status = status
	mapSeatReservations[seatReservationId] = seatTmp
}
