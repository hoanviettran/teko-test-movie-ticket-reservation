package controller

import (
	"custom-lib/infrastructure/controller"
	"custom-lib/module/healthcheck/service"
	"github.com/labstack/echo"
)

/**
 * Define a HealthCheckController
 */
type HealthCheckController struct {
	controller.BaseController
	Service service.IHealthCheckService
}

/**
 * Returns a new HealthCheckController
 */
func NewHealthCheckController(service service.IHealthCheckService) *HealthCheckController {
	return &HealthCheckController{
		Service: service,
	}
}

/**
 * Returns status
 */
func (controller *HealthCheckController) GetStatus(c echo.Context) error {
	return controller.WriteSuccessEmptyContent(c)
}

