package healthcheck

import (
	"time"

	"custom-lib/module/healthcheck/controller"
	"custom-lib/module/healthcheck/service"
	"github.com/labstack/echo"
)

var mHealthCheckController *controller.HealthCheckController

/**
 * Initializes module
 */
func Initialize(e *echo.Echo, timeout time.Duration) {
	healthCheckService := service.NewHealthCheckService(timeout)
	mHealthCheckController = controller.NewHealthCheckController(healthCheckService)

	// New router
	initRouter(e)
}

/**
 * Initializes router
 */
func initRouter(e *echo.Echo) {
	e.GET("/api/v1.0/status", mHealthCheckController.GetStatus)
	// e.GET("game/offline/api/v1.0/database/status", mHealthCheckController.GetMySqlStatus)
}
