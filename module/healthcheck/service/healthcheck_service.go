package service

import (
	"time"
)

type IHealthCheckService interface {
}

type HealthCheckService struct {
	Timeout time.Duration
}

/**
 * Returns a new HealthCheckService
 */
func NewHealthCheckService(timeout time.Duration) IHealthCheckService {
	return &HealthCheckService{
		Timeout: timeout,
	}
}

